package py.com.jtc.rest.controller;

import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import py.com.jtc.rest.bean.Cuenta;
import py.com.jtc.rest.bean.Movimiento;
import py.com.jtc.rest.bean.Transferencia;
import py.com.jtc.rest.repository.CuentaRepository;
import py.com.jtc.rest.repository.MovimientoRepository;

@RestController
@RequestMapping("/cuenta")
public class CuentaController {

	@Autowired
	private CuentaRepository cuentaRepository;

	@Autowired
	private MovimientoRepository movimientoRepository;

	@GetMapping
	public List<Cuenta> obtenerCuenta() {
		List<Cuenta> cuenta = cuentaRepository.obtenerCuenta();
		return cuenta;
	}

	@GetMapping("/{idusuario}")
	public List<Cuenta> obtenerCtaUsuario(@PathVariable("idusuario") Integer idusuario) {
		List<Cuenta> cuenta = cuentaRepository.obtenerCtaUsuario(Integer.valueOf(idusuario));
		return cuenta;
	}

	/*
	 * @GetMapping("/prueba/{idcuenta}") public List<Cuenta>
	 * obtenerUnaCta(@PathVariable("idcuenta") Integer idcuenta) { List<Cuenta>
	 * cuenta = cuentaRepository.obtenerUnaCta(Integer.valueOf(idcuenta));
	 * return cuenta; }
	 */

	@GetMapping("/transferencia/{idcuenta}")
	public Cuenta obtenerUnaCuenta(@PathVariable("idcuenta") Integer idcuenta) {
		Cuenta cuenta = cuentaRepository.obtenerUnaCuenta(idcuenta);
		return cuenta;
	}

	@PostMapping
	public ResponseEntity<Transferencia> ctaTransferencia(@RequestBody Transferencia tran,
			UriComponentsBuilder uBuilder) {

		Transferencia t = new Transferencia();
		t.setCtacredito(tran.getCtacredito());
		t.setCtadebito(tran.getCtadebito());
		t.setSaldo(tran.getSaldo());
		t.setMonto(tran.getMonto());
		t.setIdcuenta(tran.getIdcuenta());
		if (t.getMonto() > 0) {

			if (t.getSaldo() >= t.getMonto()) {

				// Actualiza la Cta Débito
				Cuenta ctaDeb = new Cuenta();
				ctaDeb.setIdcuenta(tran.getIdcuenta());
				ctaDeb.setSaldo(tran.getSaldo() - tran.getMonto());
				cuentaRepository.actualizarCuenta(ctaDeb, Integer.valueOf(ctaDeb.getIdcuenta()));

				Movimiento movDeb = new Movimiento();
				movDeb.setIdcuenta(ctaDeb.getIdcuenta());
				movDeb.setTipomovimiento("D");
				movDeb.setMonto(tran.getMonto());
				movimientoRepository.agregarMovimiento(movDeb);

				// Actualiza el Crédito
				Cuenta ctaCre = cuentaRepository.obtenerNroCuenta(tran.getCtacredito());
				ctaCre.setSaldo(ctaCre.getSaldo() + tran.getMonto());
				cuentaRepository.actualizarCuenta(ctaCre, Integer.valueOf(ctaCre.getIdcuenta()));

				Movimiento movCred = new Movimiento();
				movCred.setIdcuenta(ctaCre.getIdcuenta());
				movCred.setTipomovimiento("C");
				movCred.setMonto(tran.getMonto());
				movimientoRepository.agregarMovimiento(movCred);
			}
		}
		HttpHeaders header = new HttpHeaders();

		URI uri = uBuilder.path("/cuenta/").path(String.valueOf(t.getIdcuenta())).build().toUri();

		header.setLocation(uri);

		return new ResponseEntity<>(t, header, HttpStatus.CREATED);
	}

	@PutMapping("/{idcuenta}")
	public Cuenta editarCuenta(@PathVariable("idcuenta") Integer idcuenta, @RequestBody Cuenta cuenta,
			UriComponentsBuilder uBuilder) {
		Cuenta c = null;
		boolean result = cuentaRepository.actualizarCuenta(cuenta, Integer.valueOf(idcuenta));
		if (result) {
			c = cuentaRepository.obtenerUnaCuenta(idcuenta);
		}
		return c;
	}

}
