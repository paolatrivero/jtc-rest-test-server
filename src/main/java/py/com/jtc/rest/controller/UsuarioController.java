package py.com.jtc.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.jtc.rest.bean.Usuario;
import py.com.jtc.rest.bean.UsuarioLogin;
import py.com.jtc.rest.repository.UsuarioRepository;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@GetMapping
	public List<Usuario> obtenerUsuario() {
		List<Usuario> usuario = usuarioRepository.obtenerUsuario();
		return usuario;
	}
	
	@PostMapping
	public boolean usulogin(@RequestBody UsuarioLogin usu) {				
		
		Usuario usuario = usuarioRepository.obtenerUsuLogin(usu.getNrodocumento().toString());
				
		if (usuario != null) {
			if (usuario.getPin().equals(usu.getPin())){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	@GetMapping("/{idcuenta}")
	public Usuario obtenerUsuarioCta(@PathVariable("idcuenta") Integer idcuenta) {
		Usuario usuario = usuarioRepository.obtenerUsuarioCta(idcuenta);
		return usuario;
	}
}
