package py.com.jtc.rest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.bean.Movimiento;
import py.com.jtc.rest.repository.MovimientoRepository;

@RestController
@RequestMapping("/movimiento")
public class MovimientoController {
	@Autowired
	private MovimientoRepository movimientoRepository;
	
	@GetMapping
	public List<Movimiento> obtenerMovimiento() {
		List<Movimiento> movimiento = movimientoRepository.obtenerMovimiento();
		return movimiento;
	}
	
	@GetMapping("/{idusuario}")
	public List<Movimiento> obtenerMovCuenta(@PathVariable("idusuario") Integer idusuario) {
		List<Movimiento> movimiento = movimientoRepository.obtenerMovCuenta(Integer.valueOf(idusuario));
		return movimiento;
	}
	
	@PostMapping
	public ResponseEntity<Movimiento> guardarMovimiento(@RequestBody Movimiento movimiento, UriComponentsBuilder uBuilder) {
		Movimiento c = movimientoRepository.agregarMovimiento(movimiento);
		HttpHeaders header = new HttpHeaders();
		
		URI uri = uBuilder.path("/movimiento/" )
				.path(String.valueOf(movimiento.getIdmovimiento()))
				.build()
				.toUri();
		
		header.setLocation(uri);
		
		return new ResponseEntity<>(c, header, HttpStatus.CREATED);
	}

}
