package py.com.jtc.rest.repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.Movimiento;


@Repository
public class MovimientoRepositoryImpl implements MovimientoRepository{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Movimiento> obtenerMovimiento(){
		List<Movimiento> movimiento = jdbcTemplate.query("select * from movimientos", 
				(rs) -> {
					List<Movimiento> list = new ArrayList<>();
					while(rs.next()){
						Movimiento c = new Movimiento();
						c.setFechahora(rs.getDate("fechahora"));
						c.setIdcuenta(rs.getInt("idcuenta"));
						c.setIdmovimiento(rs.getInt("idmovimiento"));
						c.setMonto(rs.getInt("monto"));
						c.setTipomovimiento(rs.getString("tipomovimiento"));
						list.add(c);
					}
					return list;
				});
		return movimiento;
	}
	
	@Override
	public List<Movimiento> obtenerMovCuenta(Integer idcuenta){
		List<Movimiento> movimiento = jdbcTemplate.query("select * from movimientos where idcuenta = ?",
				new Object[] {idcuenta}, 
				(rs) -> {
					List<Movimiento> list = new ArrayList<>();
					while(rs.next()){
						Movimiento c = new Movimiento();
						c.setFechahora(rs.getDate("fechahora"));
						c.setIdcuenta(rs.getInt("idcuenta"));
						c.setIdmovimiento(rs.getInt("idmovimiento"));
						c.setMonto(rs.getInt("monto"));
						c.setTipomovimiento(rs.getString("tipomovimiento"));
						list.add(c);
					}
					return list;
				});
		return movimiento;
	}
	
	@Override
	public Movimiento agregarMovimiento(Movimiento movimiento) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		Date fechaActual = new Date(System.currentTimeMillis());
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, 6);
		fechaActual.setTime(c.getTimeInMillis());
		
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into movimientos values (?,now(),?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setInt(2,  movimiento.getIdcuenta());
			ps.setString(3, movimiento.getTipomovimiento());
			ps.setInt(4, movimiento.getMonto());
	        return ps;
		}; 
		
		int result = jdbcTemplate.update(psc, keyHolder);
		
		if (result > 0)
			movimiento.setIdmovimiento(keyHolder.getKey().intValue());
		
		return movimiento;
		
	}

	
}
