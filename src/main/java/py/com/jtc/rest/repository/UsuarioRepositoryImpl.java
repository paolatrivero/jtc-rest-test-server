package py.com.jtc.rest.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.Cuenta;
import py.com.jtc.rest.bean.Usuario;

@Repository
public class UsuarioRepositoryImpl implements UsuarioRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Usuario> obtenerUsuario(){
		List<Usuario> usuario = jdbcTemplate.query("select * from usuarios where idusuario = 1", 
				(rs) -> {
					List<Usuario> list = new ArrayList<>();
					while(rs.next()){
						Usuario c = new Usuario();
						c.setIdusuario(rs.getInt("idusuario"));
						c.setNombre(rs.getString("nombre"));
						c.setNrodocumento(rs.getString("nrodocumento"));
						c.setPin(rs.getString("pin"));
						list.add(c);
					}
					return list;
				});
		return usuario;
	}
	
	
	@Override
	public Usuario obtenerUsuario(Integer idusuario) {
		Usuario usuario = jdbcTemplate.queryForObject("select * from usuarios where idusuario = ?", 
				new Object[] {idusuario}, 
				(rs, rowNum) -> {
					Usuario c = new Usuario();
					c.setIdusuario(rs.getInt("idusuario"));
					c.setNombre(rs.getString("nombre"));
					c.setNrodocumento(rs.getString("nrodocumento"));
					c.setPin(rs.getString("pin"));
					return c;
		        });
		
		return usuario;
	}

	@Override
	public Usuario obtenerUsuLogin(String nrodocumento) {
		Usuario usuario = jdbcTemplate.queryForObject("select * from usuarios where nrodocumento = ?", 
				new Object[] {nrodocumento}, 
				(rs, rowNum) -> {
					Usuario c = new Usuario();
					c.setIdusuario(rs.getInt("idusuario"));
					c.setNombre(rs.getString("nombre"));
					c.setNrodocumento(rs.getString("nrodocumento"));
					c.setPin(rs.getString("pin"));
					return c;
		        });
		
		return usuario;
	}	
	
	@Override
	public Usuario obtenerUsuarioCta(Integer idcuenta) {
		Usuario usuario = jdbcTemplate.queryForObject("select u.* from usuarios u, usuarios_cuentas uc where u.idusuario = uc.idusuario and uc.idcuenta = ?", 
				new Object[] {idcuenta}, 
				(rs, rowNum) -> {
						Usuario c = new Usuario();
						c.setIdusuario(rs.getInt("idusuario"));
						c.setNombre(rs.getString("nombre"));
						c.setNrodocumento(rs.getString("nrodocumento"));
						c.setPin(rs.getString("pin"));
					return c;
		        });
		
		return usuario;
	}	

}
