package py.com.jtc.rest.repository;

import java.util.List;
import py.com.jtc.rest.bean.Cuenta;

public interface CuentaRepository {
	
	List<Cuenta> obtenerCuenta();
	Cuenta obtenerUnaCuenta(Integer idcuenta);
	Cuenta obtenerNroCuenta(String nrocuenta);
	List<Cuenta> obtenerCtaUsuario(Integer idusuario);
	//List<Cuenta> obtenerUnaCta(Integer idcuenta);
	boolean actualizarCuenta(Cuenta contacto, Integer idcuenta);	

}
