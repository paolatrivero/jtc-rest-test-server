package py.com.jtc.rest.repository;

import java.util.List;
import py.com.jtc.rest.bean.Usuario;

public interface UsuarioRepository {
	List<Usuario> obtenerUsuario();
	Usuario obtenerUsuario(Integer idusuario);
	Usuario obtenerUsuLogin(String nrodocumento);
	Usuario obtenerUsuarioCta(Integer idcuenta);
}
