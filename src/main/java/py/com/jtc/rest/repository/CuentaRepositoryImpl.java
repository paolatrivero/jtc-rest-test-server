package py.com.jtc.rest.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import py.com.jtc.rest.bean.Cuenta;

@Repository
public class CuentaRepositoryImpl implements CuentaRepository{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Cuenta> obtenerCuenta(){
		List<Cuenta> cuenta = jdbcTemplate.query("select * from cuentas c", 
				(rs) -> {
					List<Cuenta> list = new ArrayList<>();
					while(rs.next()){
						Cuenta c = new Cuenta();
						c.setIdcuenta(rs.getInt("idcuenta"));
						c.setNrocuenta(rs.getString("nrocuenta"));
						c.setTipo(rs.getString("tipo"));
						c.setMoneda(rs.getString("moneda"));
						c.setSaldo(rs.getInt("saldo"));
						list.add(c);
					}
					return list;
				});
		return cuenta;
	}
	
	@Override
	public Cuenta obtenerUnaCuenta(Integer idcuenta) {
		Cuenta cuenta = jdbcTemplate.queryForObject("select * from cuentas where idcuenta = ?", 
				new Object[] {idcuenta}, 
				(rs, rowNum) -> {
					Cuenta c = new Cuenta();
					c.setIdcuenta(rs.getInt("idcuenta"));
					c.setNrocuenta(rs.getString("nrocuenta"));
					c.setTipo(rs.getString("tipo"));
					c.setMoneda(rs.getString("moneda"));
					c.setSaldo(rs.getInt("saldo"));
					return c;
		        });
		
		return cuenta;
	}
	
	@Override
	public Cuenta obtenerNroCuenta(String nrocuenta) {
		Cuenta cuenta = jdbcTemplate.queryForObject("select * from cuentas where nrocuenta = ?", 
				new Object[] {nrocuenta}, 
				(rs, rowNum) -> {
					Cuenta c = new Cuenta();
					c.setIdcuenta(rs.getInt("idcuenta"));
					c.setNrocuenta(rs.getString("nrocuenta"));
					c.setTipo(rs.getString("tipo"));
					c.setMoneda(rs.getString("moneda"));
					c.setSaldo(rs.getInt("saldo"));
					return c;
		        });
		
		return cuenta;
	}
	
	/*@Override
	public List<Cuenta> obtenerUnaCta(Integer idcuenta){
		List<Cuenta> cuenta = jdbcTemplate.query("select * from cuentas where idcuenta = ?",
				new Object[] {idcuenta}, 
				(rs) -> {
					List<Cuenta> list = new ArrayList<>();
					while(rs.next()){
						Cuenta c = new Cuenta();
						c.setIdcuenta(rs.getInt("idcuenta"));
						c.setNrocuenta(rs.getString("nrocuenta"));
						c.setTipo(rs.getString("tipo"));
						c.setMoneda(rs.getString("moneda"));
						c.setSaldo(rs.getInt("saldo"));
						list.add(c);
					}
					return list;
				});
		return cuenta;
	}*/
	
	@Override
	public List<Cuenta> obtenerCtaUsuario(Integer idusuario){
		List<Cuenta> cuenta = jdbcTemplate.query("select c.* from cuentas c, usuarios_cuentas uc where c.idcuenta = uc.idcuenta and uc.idusuario = ?",
				new Object[] {idusuario}, 
				(rs) -> {
					List<Cuenta> list = new ArrayList<>();
					while(rs.next()){
						Cuenta c = new Cuenta();
						c.setIdcuenta(rs.getInt("idcuenta"));
						c.setNrocuenta(rs.getString("nrocuenta"));
						c.setTipo(rs.getString("tipo"));
						c.setMoneda(rs.getString("moneda"));
						c.setSaldo(rs.getInt("saldo"));
						list.add(c);
					}
					return list;
				});
		return cuenta;
	}
	
	@Override
	public boolean actualizarCuenta(Cuenta cuenta, Integer idcuenta) {
		int result = jdbcTemplate.update("update cuentas set saldo = ? where idcuenta = ?", 
				
				cuenta.getSaldo(),
		        cuenta.getIdcuenta());
		return (result > 0) ? true : false;
	}

}
