package py.com.jtc.rest.bean;

public class Transferencia {
	  private Integer idcuenta;
	  private String ctadebito;
	  private String ctacredito;
	  private String moneda;
	  private Integer saldo;
	  private Integer monto;
	public Integer getIdcuenta() {
		return idcuenta;
	}
	public void setIdcuenta(Integer idcuenta) {
		this.idcuenta = idcuenta;
	}
	public String getCtadebito() {
		return ctadebito;
	}
	public void setCtadebito(String ctadebito) {
		this.ctadebito = ctadebito;
	}
	public String getCtacredito() {
		return ctacredito;
	}
	public void setCtacredito(String ctacredito) {
		this.ctacredito = ctacredito;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public Integer getSaldo() {
		return saldo;
	}
	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}
	public Integer getMonto() {
		return monto;
	}
	public void setMonto(Integer monto) {
		this.monto = monto;
	}
	  
	  
}
