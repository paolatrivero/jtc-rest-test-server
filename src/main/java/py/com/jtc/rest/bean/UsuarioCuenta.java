package py.com.jtc.rest.bean;

public class UsuarioCuenta {

	  private Integer idfirma;
	  private Integer idusuario;
	  private Integer idcuenta;
	public Integer getIdfirma() {
		return idfirma;
	}
	public void setIdfirma(Integer idfirma) {
		this.idfirma = idfirma;
	}
	public Integer getIdusuario() {
		return idusuario;
	}
	public void setIdusuario(Integer idusuario) {
		this.idusuario = idusuario;
	}
	public Integer getIdcuenta() {
		return idcuenta;
	}
	public void setIdcuenta(Integer idcuenta) {
		this.idcuenta = idcuenta;
	}
	  
	  
}
