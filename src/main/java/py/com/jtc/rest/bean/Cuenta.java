package py.com.jtc.rest.bean;

public class Cuenta {
	  private Integer idcuenta;
	  private String nrocuenta;
	  private String tipo;
	  private String moneda;
	  private Integer saldo;
	public Integer getIdcuenta() {
		return idcuenta;
	}
	public void setIdcuenta(Integer idcuenta) {
		this.idcuenta = idcuenta;
	}
	public String getNrocuenta() {
		return nrocuenta;
	}
	public void setNrocuenta(String nrocuenta) {
		this.nrocuenta = nrocuenta;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public Integer getSaldo() {
		return saldo;
	}
	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}
	  
	  
}
