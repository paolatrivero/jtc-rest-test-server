insert into empleados(id, nombre, edad, salario) values (1, 'Juan' , 30, 2500000);
insert into empleados(id, nombre, edad, salario) values (2, 'Pablo', 23, 1500000);
insert into empleados(id, nombre, edad, salario) values (3, 'Pedro', 32, 3000000);
insert into usuarios (idusuario, nrodocumento, nombre, pin) 
values (1, '147258', 'Bruce Wayne', '1234');


insert into usuarios (idusuario, nrodocumento, nombre, pin) 
values (2, '123456', 'Tony Stark', '1234');


insert into usuarios (idusuario, nrodocumento, nombre, pin) 
values (3, '654321', 'Jhon Connor', '1234');


insert into cuentas (idcuenta, nrocuenta, tipo, moneda, saldo)
values (1, '123456789', 'CUENTA CORRIENTE', 'Gs.', '1500000');


insert into cuentas (idcuenta, nrocuenta, tipo, moneda, saldo)
values (2, '298765432', 'CAJA DE AHORRO', 'Gs.', '8500000');


insert into cuentas (idcuenta, nrocuenta, tipo, moneda, saldo)
values (3, '187567522', 'CUENTA CORRIENTE', 'Gs.', '3500000');


insert into cuentas (idcuenta, nrocuenta, tipo, moneda, saldo)
values (4, '193527683', 'CUENTA CORRIENTE', 'Gs.', '800000');


insert into usuarios_cuentas (idfirma, idusuario, idcuenta) 
values (1, 1, 1);


insert into usuarios_cuentas (idfirma, idusuario, idcuenta) 
values (2, 1, 2);


insert into usuarios_cuentas (idfirma, idusuario, idcuenta) 
values (3, 2, 3);


insert into usuarios_cuentas (idfirma, idusuario, idcuenta) 
values (4, 3, 4);


insert into movimientos (idmovimiento, fechahora, idcuenta, tipomovimiento, monto)
values (1, now(), 1, 'D', '70000');


insert into movimientos (idmovimiento, fechahora, idcuenta, tipomovimiento, monto)
values (2, now(), 2, 'C', '50000');

insert into movimientos (idmovimiento, fechahora, idcuenta, tipomovimiento, monto)
values (3, now(), 2, 'C', '80000');